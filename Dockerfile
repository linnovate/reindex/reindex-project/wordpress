ARG BUNDLE_IMAGE_REF=""

FROM $BUNDLE_IMAGE_REF AS bundle

RUN echo $REINDEX_PLUGIN_FOLDER;

FROM wordpress:5.3.2-php7.2-fpm

ARG REINDEX_PLUGIN_FOLDER="reindex-ui"

RUN apt-get update

RUN yes | apt-get install vim 

ARG CACHEBUST=1

RUN echo $CACHEBUST;

COPY --chown=www-data:www-data . /var/www/html

COPY --from=bundle usr/src/app/dist /var/www/html/wp-content/plugins/$REINDEX_PLUGIN_FOLDER/assets/client-render

COPY --chown=www-data:www-data ./wp-config.php /var/www/html

RUN chown 777 /var/www/html/wp-content

WORKDIR /var/www/html
