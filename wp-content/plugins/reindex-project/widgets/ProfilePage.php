<?php
namespace Project;

use \Elementor\Controls_Manager;

class Widget_ProfilePage extends AbstractWidget
{
    public function get_name()
    {
        return 'ProfilePage';
    }

    public function get_title()
    {
        return __('Profile Page', 'reindex-project');
    }

    public function get_icon()
    {
        return 'eicon-product-categories';
    }


    protected function _register_controls()
    {

        /* ================================== General ================================== */
        
        $this->start_controls_section('general', array(
            'label' => __('General', 'reindex-project'),
        ));

	$this->add_control('pageTitle', array(
		'type' => Controls_Manager::TEXT,
			'label'=>__('Page Title:', 'reindex-project'),
			'default' => __('Hop on!','reindex-project'),
	));

        $this->end_controls_section();


    }
}
