<?php
namespace Project;

use \Elementor\Controls_Manager;

class Widget_WidgetsStorage extends AbstractWidget
{
    public function get_name()
    {
        return 'WidgetsStorage';
    }

    public function get_title()
    {
        return __('Widgets Storage', 'reindex-project');
    }

    public function get_icon()
    {
        return 'eicon-product-categories';
    }


    protected function _register_controls()
    {

        /* ================================== General ================================== */
        
        $this->start_controls_section('general', array(
            'label' => __('General', 'reindex-project'),
        ));

        $this->add_control('backLabel', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Back label:', 'reindex-project'),
            'default' => __('Back to search results', 'reindex-project'),
        ));

        $this->add_control('backRedirect', array(
            'type'        => Controls_Manager::URL,
            'label'       => __('Button redirect:', 'reindex-project'),
//             'description' => __('The url redirection after search submit.', 'reindex-project'),
            'placeholder' => __( 'https://your-link.com', 'reindex-project' ),
            'show_external' => true,
			'default' => [
				'url' => '/results',
				'is_external' => false,
				'nofollow' => false,
			],
        ));

        $this->add_control('searchPlaceholder', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Search placeholder:', 'reindex-project'),
            'default' => __('What do you need help with ?', 'reindex-project'),
            'description' => __('The placeholder inside search input field.', 'reindex-project'),
        ));

        $this->add_control('searchRedirect', array(
            'type'        => Controls_Manager::URL,
            'label'       => __('Button redirect:', 'reindex-project'),
            'description' => __('The url redirection after search submit.', 'reindex-project'),
            'placeholder' => __( 'https://your-link.com', 'reindex-project' ),
            'show_external' => true,
			'default' => [
				'url' => '/results',
				'is_external' => false,
				'nofollow' => false,
			],
        ));

        $this->end_controls_section();


        /* ================================== Sidebar ================================== */
        
        $this->start_controls_section('detail', array(
            'label' => __('Details Labels', 'reindex-project'),
        ));

        $this->add_control('detailContact', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Contact:', 'reindex-project'),
            'default' => __('contact me', 'reindex-project'),
        ));

        $this->add_control('detailContactAgency', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Contact agency:', 'reindex-project'),
            'default' => __('contact us', 'reindex-project'),
        ));

        $this->add_control('detailTitleExperties', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Experties:', 'reindex-project'),
            'default' => __('EXPERTIES', 'reindex-project'),
        ));

        $this->add_control('detailTitleLocation', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Location:', 'reindex-project'),
            'default' => __('Location', 'reindex-project'),
        ));

        $this->add_control('detailTitlePrices', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Prices:', 'reindex-project'),
            'default' => __('PRICES', 'reindex-project'),
        ));

        $this->add_control('detailTitleDetails', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Details:', 'reindex-project'),
            'default' => __('Details', 'reindex-project'),
        ));

        $this->add_control('detailTitleFindMe', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Find me:', 'reindex-project'),
            'default' => __('You can find me here', 'reindex-project'),
        ));

        $this->add_control('detailLanguages', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Languages:', 'reindex-project'),
            'default' => __('Details', 'reindex-project'),
        ));

        $this->add_control('detailTeamSize', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Team size:', 'reindex-project'),
            'default' => __('Details', 'reindex-project'),
        ));

        $this->add_control('detailTimeZone', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Time zone:', 'reindex-project'),
            'default' => __('Time zone', 'reindex-project'),
        ));

        $this->add_control('detailAvgProjectRate', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Avg project rate:', 'reindex-project'),
            'default' => __('Avg project rate', 'reindex-project'),
        ));

        $this->add_control('detailHourlyRate', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Hourly rate:', 'reindex-project'),
            'default' => __('Hourly rate', 'reindex-project'),
        ));

        $this->add_control('detailPricePproject', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Price project:', 'reindex-project'),
            'default' => __('Price per project', 'reindex-project'),
        ));

        $this->end_controls_section();


        /* ================================== Tab - About ================================== */
        
        $this->start_controls_section('aboutTab', array(
            'label' => __('Tab - About', 'reindex-project'),
        ));


        $this->add_control('aboutTabTitle', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Title:', 'reindex-project'),
            'default' => __('About', 'reindex-project'),
        ));
    
        $this->end_controls_section();


        /* ================================== Tab - Portfolio ================================== */
        
        $this->start_controls_section('portfolioTab', array(
            'label' => __('Tab - Portfolio', 'reindex-project'),
        ));


        $this->add_control('portfolioTabTitle', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Title:', 'reindex-project'),
            'default' => __('Portfolio', 'reindex-project'),
        ));

        $this->add_control('portfolioTabAddItem', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Tab - About:', 'reindex-project'),
            'default' => __('add Portfolio item', 'reindex-project'),
        ));
     
        $this->end_controls_section();
    }
}
