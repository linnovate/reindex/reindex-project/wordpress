<?php
namespace Project;

use \Elementor\Controls_Manager;

class Widget_Header extends AbstractWidget
{
    public function get_name()
    {
        return 'Header';
    }

    public function get_title()
    {
        return __('Header', 'reindex-project');
    }

    public function get_icon()
    {
        return 'eicon-product-categories';
    }


    protected function _register_controls()
    {

        /* ================================== General ================================== */
        
        $this->start_controls_section('general', array(
            'label' => __('General', 'reindex-project'),
        ));

        $this->end_controls_section();


    }
}
