<?php
namespace Project;

use \Elementor\Controls_Manager;

class Widget_Footer extends AbstractWidget
{
    public function get_name()
    {
        return 'Footer';
    }

    public function get_title()
    {
        return __('Footer', 'reindex-project');
    }

    public function get_icon()
    {
        return 'eicon-product-categories';
    }


    protected function _register_controls()
    {

        /* ================================== General ================================== */
        
        $this->start_controls_section('general', array(
            'label' => __('General', 'reindex-project'),
        ));

        $this->add_control('text', array(
            'type'        => Controls_Manager::TEXT,
            'label'       => __('Footer Text', 'reindex-project'),
            'default' => __('Powered by Reindex', 'reindex-project'),
        ));


        $this->end_controls_section();


    }
}
