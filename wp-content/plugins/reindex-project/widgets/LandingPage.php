<?php
namespace Project;

use \Elementor\Controls_Manager;

class Widget_LandingPage extends AbstractWidget
{
    public function get_name()
    {
        return 'LandingPage';
    }

    public function get_title()
    {
        return __('Landing page', 'reindex-project');
    }

    public function get_icon()
    {
        return 'eicon-product-categories';
    }


    protected function _register_controls()
    {

        /* ================================== General ================================== */
        
        $this->start_controls_section('general', array(
            'label' => __('General', 'reindex-project'),
        ));


        $this->end_controls_section();


    }
}
