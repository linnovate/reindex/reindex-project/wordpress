<?php
namespace Project;

use \Elementor\Controls_Manager;

class Widget_RegisterPage extends AbstractWidget
{
    public function get_name()
    {
        return 'RegisterPage';
    }

    public function get_title()
    {
        return __('Register Page', 'reindex-project');
    }

    public function get_icon()
    {
        return 'eicon-product-categories';
    }


    protected function _register_controls()
    {

        /* ================================== General ================================== */
        
        $this->start_controls_section('general', array(
            'label' => __('General', 'reindex-project'),
        ));

	$this->add_control('pageTitle', array(
		'type' => Controls_Manager::TEXT,
			'label'=>__('Page Title:', 'reindex-project'),
			'default' => __('Hop on!','reindex-project'),
	));

        $this->end_controls_section();
	
        /* ================================== Form ================================== */
        $this->start_controls_section('form', array(
            'label' => __('Form', 'reindex-project'),
        ));

	$this->add_control('formFields', array(
		'type' => Controls_Manager::TEXT,
			'label'=>__('Form Fields:', 'reindex-project'),
			'default' => __('firstName,lastName,familyStatus,email,dateOfBirth,homeAddress,phoneNumber,password','reindex-project'),
	));
	$this->add_control('formFieldsLabels', array(
		'type' => Controls_Manager::TEXT,
			'label'=>__('Fields Labels:', 'reindex-project'),
			'default' => __('First Name,Last Name,Family Status,Email,Date of birth,Home Address,Phone number,Password','reindex-project'),
	));

        $this->end_controls_section();

    }
}
