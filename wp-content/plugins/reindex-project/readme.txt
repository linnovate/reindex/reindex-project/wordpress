=== Elementor Experts ===
Contributors: Linnovate
Donate link: https://linnovate.net/
Tags: elementor
Requires at least: 4.0
Tested up to: 4.9
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Experts widgets for Elementor.com
 
This plugin requires:

* [Elementor Page Builder](https://wordpress.org/plugins/elementor/).

== Installation ==

1. Upload the ElementorExperts plugin to the /wp-content/plugins/ directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. set in http://localhost:8080/wp-admin/options-general.php?page=client-render 
```
    /wp-content/plugins/elementor-experts/assets/client-render/react_components.bundle.js
```

== Screenshots ==


== Changelog ==

