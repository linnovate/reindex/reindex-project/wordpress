<?php
namespace Project;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class ProjectSettings
{
    	
    public function __construct()
    {
		
        add_action( 'admin_menu', array( $this, 'reindex_project_setup_menu' ) );
		add_action( 'admin_init', array( $this, 'reindex_project_setup_init' ) );
		

    }

/**
	 * add settings sections and fields for the config page of the plugin in admin panel
	 */
	public function reindex_project_setup_init(){
        add_settings_section("reindex_project_settings", "Reindex Project", array($this, 'section_callback'), "reindex_project_fields");
       
	   // title field
	    add_settings_field( 'title', 'Title: ', array( $this, 'display_title_form_element' ), 'reindex_project_fields', "reindex_project_settings" );
	    register_setting("reindex_project_settings", "title");
       

    }

	
    /**
	 * function: elementor_experts_setup_menu
	 *
	 * creating a menu item in the admin menu for the plugin
	 */
	public function reindex_project_setup_menu(){

		$hook = add_menu_page( 'Reindex Project settings', 'Reindex Project', 'manage_options', 'reindex-project', array($this,'elementor_config_form' ));

	} 


    /**
	 * create the title field in the config page
	 */
    public function display_title_form_element(){ ?>
		 <input type="text" name="title" id="title" value="<?php echo get_option('title'); ?>" />
    <?php
    }

	
	/**
	 * section callback function from add_settings_section function
	 * we can add here description and etc. as the example in the comment
	 * we are keeping this for an example
	 */
    public function section_callback( $arguments ) {
        /* Set up input*/
      /*  switch( $arguments['id'] ){
            case "title" :
                echo "Categories that will trigger the member only message.";
                break;
            break;
        } */
    }

	

	/**
	 * function: elementor_config_form
	 *
	 * creating a config form for the plugin
	 */
	public function elementor_config_form(){
		?>
		<div>
		
		<h1>Reindex Project Settings</h1>
		<form method="post" action="options.php">
		<?php 
			settings_fields("reindex_project_settings");
            do_settings_sections("reindex_project_fields");
            submit_button();
		?>
		</form>
		</div>
		<?php
        
	}





 	// sending the settings values when reaching the route:
	// /wp-json/experts/v1/api/settings 
	//
	public function get_project_settings_json(){

		$title = get_option('title');

		$settings_json = json_encode(array(
			"title" => $title,
		));

		
		echo $settings_json;
		
		return rest_ensure_response( $settings_json);
	}
}
