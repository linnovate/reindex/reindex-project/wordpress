<?php
/*
 * Plugin Name: Reindex Project
 * # Plugin URI: https://linnovate.net/
 * Description: Manage Reindex Project in Elementor builder. 
 * Version: 1.0.3
 * Author: Linnovate
 * Author URI: https://linnovate.net/
 * License: GPLv2 or later
 * Text Domain: reindex-project
 */
if (!defined('ABSPATH')) {
    exit('Press Enter to proceed...');
}
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if( !is_plugin_active('wordpress-client-render/client-render.php') || !is_plugin_active('elementor/elementor.php') ) {
	add_action( 'admin_notices', function() {
		printf('<div class="notice notice-error"><p>
			<strong>Reindex-Project</strong> plugin is <strong>deactivate</strong> (client-render & elementor plugins is Required) !
		<p></div>');
  });
	deactivate_plugins( plugin_basename( __FILE__ ) );
} else {
  require __DIR__ . '/plugin.php';
}
