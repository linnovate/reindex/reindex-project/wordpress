<?php
namespace Project;

require __DIR__ . '/includes/reindex-settings.php';

if (!defined('ABSPATH')) {
	exit('Press Enter to proceed...');
}

/**
 * ReindexProject plugin.
 *
 * @since 1.0.0
 */
final class ReindexProject {

	/**
	 * Widgets filename.
	 *
	 * get the all ElementorExperts widgets by the filename, which is in: __DIR__/widgets/).
	 * 
	 * The "filename" Must match there class name similar to - search-box => Widget_SearchBox
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @var widgets_filename
	 */
	private $widgets_filename = [
		'LandingPage',
		'LoginPage',
		'RegisterPage',
		'ProfilePage',
		'EditPage',
		'FamilyPage',
		'InvitePage',
		'JoinPage',
		'WidgetsStorage',
		'Header',
		'Footer'
	];


	/**
	 * Instance.
	 *
	 * Holds the plugin instance.
	 *
	 * @since 1.0.0
	 * @access private
	 * @static
	 *
	 * @var $instance
	 */
	private static $_instance = null;

	/**
	 * Settings.
	 *
	 * Holds the plugin settings.
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @var Settings
	 */
	private static  $settings = null;

	/**
	 * Register Categories.
	 *
	 * Add a new widget type to the list of registered widget types.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function registerCategories()
	{
		\Elementor\Plugin::instance()->elements_manager->add_category(
			'reindex-project',
			array(
				'title' => __('Reindex Project', 'reindex-project'),
				'icon'  => 'fa fa-plug'
			)
		);


	}


	/**
	 * Register widget type.
	 *
	 * Add a new widget type to the list of registered widget types.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 */
	public function registerWidgets() {

		require __DIR__ . '/widgets/AbstractWidget.php';

		foreach ( $this->widgets_filename as $widget_filename ) {

			include( __DIR__ . '/widgets/' . $widget_filename . '.php' );
			$class_name = str_replace( '-', '_', $widget_filename );
			$class_name = __NAMESPACE__ . '\Widget_' . $class_name;
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new $class_name() );
		}

	}

	/**
	 * Instance.
	 *
	 * Ensures only one instance of the plugin class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @return Plugin An instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * loading scripts on init
	 */
	public function reindex_project_load_scripts_admin(){
		wp_enqueue_script( 'reindex-project', plugin_dir_url( __FILE__ ) . 'assets/js/reindex-project.js', array('jquery'), false, false );
		// remove default style
		add_action('wp_enqueue_scripts', function() {
			wp_dequeue_style( 'twentynineteen-style' );
			wp_dequeue_style( 'experts-style' );
			wp_dequeue_style( 'hello-elementor-child' );
			wp_dequeue_style( 'hello-elementor' );

		}, 100);

		wp_enqueue_media();
	}

	public static function reindex_project_load_settings(){
		if (is_null(self::$settings)) {
			self::$settings = new ProjectSettings();
		}
	}

	/**
	 * Plugin constructor.
	 *
	 * Initializing Elementor Experts plugin.
	 *
	 * @since 1.0.0
	 * @access private
	 */
	private function __construct() {
		// Check required version
		if (!version_compare(ELEMENTOR_VERSION, '1.8.0', '>=')) {
			return;
		}
		

		// initiate the plugin
		add_action( 'plugins_loaded', [ $this, 'init' ] );

		//
		// register a new route for getting the settings values
		//
		// add_action('rest_api_init', function () {
		// 	register_rest_route( 'project/v1/api', 'settings',array(
		// 		'methods'  => 'GET',
		// 		'callback' => array($this,'get_project_settings_json'),
		// 		'permission_callback' => function () {
		// 			// to implement later
		// 			return true;
		// 		}
		// 	));  
		// }); 
	}

	public function init() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
			return;
		}

		add_action( 'admin_enqueue_scripts', array($this, 'reindex_project_load_scripts_admin' ));
		add_action( 'wp_enqueue_scripts', array($this, 'reindex_project_load_scripts_admin' ));

		add_action( 'elementor/init', array($this, 'registerCategories') );
		add_action( 'elementor/widgets/widgets_registered', array($this, 'registerWidgets') );

	}

	// sending the settings values when reaching the route:
	// /wp-json/experts/v1/api/settings 
	//
	// public function get_project_settings_json(){
	// 	if(is_null(self::$settings)){
	// 		self::$settings = new ProjectSettings();
	// 	}

	// 	self::$settings->get_project_settings_json(); 
	// }

}
ReindexProject::instance();

// add_action('elementor/loaded', function() {
// 	ReindexProject::instance();
// 	if(is_admin()){
// 		ReindexProject::reindex_project_load_settings();

// 	}

// });


add_action('admin_menu', function() {	
	// $page_name, $menu_slug, $component_name, $uuid,
	\ClientRender\Plugin::$instance->add_menu_page('Reindex', 'reindex-config', 'AdminPage', 'admin_page');

	// $parent_slug, $page_name, $menu_slug, $component_name, $uuid,
	// \ClientRender\Plugin::$instance->add_submenu_page('reindex-config', 'Filters', 'reindex-config-filters', 'AdminFilters', 'admin_filtes');

	// \ClientRender\Plugin::$instance->add_submenu_page('reindex-config', 'Search', 'reindex-config-search', 'AdminSearchHandler', 'admin_search_handler');

	// Search
		\ClientRender\Plugin::$instance->add_submenu_page(
			'reindex-config', //parent_slug
			'Project Search', //page_name
			'reindex-config-search', //menu_slug
			'ProjectAdminSearch', //component_name
			'admin_search' //uuid
		);

});




