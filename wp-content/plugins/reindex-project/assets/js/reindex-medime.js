/*
 * CRRender
 *
 */

jQuery(document).ready(function($) {
  var custom_uploader;
  if ($("#cover_img").val()) {
    $("#remove_image_button").show();
  } else {
    $("#remove_image_button").hide();
  }
  // The image "Upload" button in the settings pahge
  $("#upload_image_button").click(function(e) {
    e.preventDefault();

    //If the uploader object has already been created, reopen the dialog
    if (custom_uploader) {
      custom_uploader.open();
      return;
    }

    //Extend the wp.media object
    custom_uploader = wp.media.frames.file_frame = wp.media({
      title: "Choose Image",
      button: {
        text: "Choose Image"
      },
      multiple: false
    });

    //When a file is selected, grab the URL and set it as the text field's value
    custom_uploader.on("select", function() {
      attachment = custom_uploader
        .state()
        .get("selection")
        .first()
        .toJSON();
      $("#cover_img").val(attachment.url);
      $("#cover_img_preview").attr("src", attachment.url);
      $("#remove_image_button").show();
    });

    //Open the uploader dialog
    custom_uploader.open();
  });

  // The "Remove" image button (remove the value from input type='hidden') in the settings page
  $("#remove_image_button").click(function() {
    var answer = confirm("Are you sure?");
    if (answer == true) {
      $("#cover_img").val("");
      $("#remove_image_button").hide();
      $("#cover_img_preview").attr("src", "");
    }
    return false;
  });
});
