<?php

/*
 * Plugin Name: Client Render
 * # Plugin URI: https://linnovate.net/
 * Description: Manage Client-Render for Wordpress.
 * Version: 1.0.2
 * Author: Linnovate
 * Author URI: https://linnovate.net/
 * License: GPLv2 or later
 * Text Domain: client-render
 */

if (!defined('ABSPATH')) {
    exit('Press Enter to proceed...');
}

require __DIR__ . '/includes/settings.php';

require __DIR__ . '/plugin.php';
